#!/bin/sh

export NODE_ENV=default
export PUERTO=3000

#validación jwt
export TOKEN_SECRET=tokenultrasecreto

# monitoreo
export KAFKA_USERNAME=uamserv01
export KAFKA_DOMAIN=DESA.BESTADO.CL
export KAFKA_BROKERS=udetd704.banco.bestado.cl:9093
export KAFKA_KEYTAB=uamserv.keytab


# redis
export KEY_SECRET=keyultrasecreto
export REDIS_EXPIRATION=300

