const cache = require("redis-bech").cache;

const enviarJournal = require("../modules/enviarJournal").enviarJournal;

const enviarLog = require("../modules/enviarMonitoreo").enviarLog;

const config = require("../config/config");
async function clientes(req, res) {
  /* Ejemplo envío de log de monitoreo */
  let rut = "";
  let dv = "";
  let fechaInicio = new Date();
  let fechaFin = new Date();
  let token = "token JWT";
  enviarLog(req, res, rut, dv, fechaInicio, fechaFin, token);

  /* Ejemplo uso journal (linealizado) */
  let tipoArtefacto = "microservicio";
  let dataVariable1 = "data";
  let dataVariable2 = "data";
  let dataVariable3 = "data";

  enviarJournal(
    req,
    tipoArtefacto,
    fechaInicio,
    fechaFin,
    rut,
    null,
    null,
    null,
    dataVariable1,
    dataVariable2,
    dataVariable3
  );

  /* Ejemplo de uso redis */
  let setCache = cache.guardarValorTimeoutCacheEncriptado;
  let codigosesion = req.headers.codigosesion;
  let datosCache = {
    codigosesion: codigosesion
  };
  // Guardado cache
  setCache(codigosesion, JSON.stringify(datosCache), config.redis.expiration);

  return res.status(200).send("clientes");
}

module.exports = { clientes };
