const journal = require("journal-bech").journal;

/**
 * TODO: integrar con journal-bech
 *
 * Formatea la fecha para ser parseada en Journalizado
 * @param {date} date
 * @returns {String}
 */
function fechaJournal(date) {
  // Formato de fecha Journal: yyyy-mm-dd hh.mm.ss.ms
  let year = date.getFullYear();
  let mes = date.getMonth() + 1;
  mes = ("0" + mes).slice(-2); // Padding

  let dia = date.getDate();
  dia = ("0" + dia).slice(-2);

  let hora = date.getHours();
  hora = ("0" + hora).slice(-2);

  let minuto = date.getMinutes();
  minuto = ("0" + minuto).slice(-2);

  let segundo = date.getSeconds();
  segundo = ("0" + segundo).slice(-2);

  let milisegundo = date.getMilliseconds();
  milisegundo = ("00" + milisegundo).slice(-3);

  return (
    year +
    "-" +
    mes +
    "-" +
    dia +
    " " +
    hora +
    "." +
    minuto +
    "." +
    segundo +
    "." +
    milisegundo
  );
}

/**
 * TODO: integrar con journal-bech
 *
 * objeto formateado para enviar a Journalizado
 *
 * @param {date} fechaInicio     (objeto Date)
 * @param {date} fechaFin        (objeto Date)
 * @param {string} rut           (sin puntos ni dígito)
 * @param {string} codigosesion
 * @param {obj} req              (objeto request del controlador)
 * @param {obj} err              ('001' en caso de error, '000' en caso OK)
 * @param {string} codErr        (tripleta de error monitoreo funcional)
 * @param {string} msgErr        (glosa del error)
 * @param {string} funcionalidad (e.g. "AU001")
 * @param {string} aplicacion    (e.g. "AUTENTICACION")
 * @param {string} canal         ('43')
 * @param {string} datVariable1  (definido por el usuario)
 * @param {string} datVariable2  (definido por el usuario)
 * @param {string} datVariable3  (definido por el usuario)
 * @returns {nm$_enviarJournal.journalMsg.enviarJournalAnonym$0}
 */
class JournalMsg {
  constructor(
    fechaInicio,
    fechaFin,
    rut,
    codigosesion,
    req,
    err,
    codErr,
    msgErr,
    funcionalidad,
    aplicacion,
    canal = "43",
    datVariable1,
    datVariable2,
    datVariable3
  ) {
    this.fechaInicio = fechaJournal(fechaInicio);
    this.fechaFin = fechaJournal(fechaFin);
    this.canal = canal;
    this.funcionalidad = funcionalidad;
    this.rut = rut;
    this.sessionId = codigosesion;
    this.ipUsuario =
      req.headers["X-Forwarded-For"] || req.connection.remoteAddress;
    this.navegador = req.headers["user-agent"];
    this.aplicacion = aplicacion;
    this.error = err;
    this.glosaDeError = codErr + " - " + msgErr;
    this.dataVariable1 = datVariable1; // Renombrar según corresponda
    this.dataVariable2 = datVariable2;
    this.dataVariable3 = datVariable3;
  }
}

function enviarJournal(
  req,
  tipoArtefacto,
  fechaInicio,
  fechaFin,
  rut,
  err,
  codErr,
  msgErr,
  datVariable1,
  datVariable2,
  datVariable3
) {
  // Formato
  var msg = new JournalMsg(
    fechaInicio,
    fechaFin,
    rut,
    req.haders.codigosesion,
    req,
    err,
    codErr,
    msgErr,
    req.headers.funcionalidad,
    req.headers.aplicacion,
    req.headers.canal,
    datVariable1,
    datVariable2,
    datVariable3
  );

  // Esto genera un evento de envío de log
  journal(msg, tipoArtefacto); // TipoArtefacto = BFF, Microservicio, etc
}

module.exports = { enviarJournal };
