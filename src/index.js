"use strict";

const express = require("express");
require("express-async-errors");
const helmet = require("helmet");
const { routes } = require("./routes/routes");
const bodyParser = require("body-parser");
const logger = require("logger-bech").loggerBECH;
const config = require("./config/config");

const cache = require("redis-bech").cache;

const app = express();

async function serverStart() {
  app.use(bodyParser.json());

  // Seguridad; ver https://github.com/helmetjs/helmet
  app.use(helmet());

  // Incorporar rutas
  routes(app);

  await cache.crearYconectarCache(); // Conexión a redis

  let port = config.microservicios.port;
  app.listen(port, () => {
    logger.info("Servidor ejecutandose en el puerto: " + port);
  });
}

async function cleanup() {
  logger.info("Desconectando...");

  await cache.desconectarCache();

  process.exit(0);
}

// Captura de señales de S.O. en las que ejecutaremos el cierre de conexiones
process.on("SIGTERM", cleanup);
process.on("SIGINT", cleanup);
process.on("SIGHUP", cleanup);

serverStart();
