const { validarPasoMDW } = require("jwt-bech");

const validarHeaders = require("../modules/validarHeadersMDW");

const { clientes } = require("../controllers/clientes.controller");

const { cuentas } = require("../controllers/cuentas.controller");

const errorHandler = require("../modules/errorHandler");

function routes(app) {
  app.use("/microservicio/v0/pruebaMS/clientes", validarPasoMDW);

  app.use("/microservicio/v0/pruebaMS/clientes", validarHeaders);

  app.get("/microservicio/v0/pruebaMS/clientes", clientes);

  app.use("/microservicio/v0/pruebaMS/cuentas", validarPasoMDW);

  app.use("/microservicio/v0/pruebaMS/cuentas", validarHeaders);

  app.get("/microservicio/v0/pruebaMS/cuentas", cuentas);

  // Middleware para manejo de errores
  app.use(errorHandler);
}

module.exports = { routes };
