# pruebaMS

Prueba de eskeleto

1. [Configuración](/readme.md#1-ownership)
2. [Operaciones](/readme.md#2-operaciones)
3. [Ownership](/readme.md#3-api-spec)

## Configuración

## Ejecución local

Para ejecutar localmente el servicio se utiliza el siguiente comando :
```bash
npm run local
```
éste se encarga de cargar las variables de entorno definidas en scripts/variables_entorno.sh
e invocar al proceso node

### Variables de entorno

Las siguientes variables de entorno se encuentran definidas
en el archivo scripts/variables_entorno.sh

generales

* NODE_ENV=default
* PUERTO=3000

validación jwt

* TOKEN_SECRET=tokenultrasecreto

monitoreo

* KAFKA_USERNAME=uamserv01
* KAFKA_DOMAIN=DESA.BESTADO.CL
* KAFKA_BROKERS=udetd704.banco.bestado.cl:9093
* KAFKA_KEYTAB=uamserv.keytab


redis
* REDIS_EXPIRATION = 300
* KEY_SECRET=keyultrasecreto


## Operaciones


### clientes

#### ruta : microservicio/v0/pruebaMS/clientes

#### descripción

#### parámetros de entrada

#### salida


### cuentas

#### ruta : microservicio/v0/pruebaMS/cuentas

#### descripción

#### parámetros de entrada

#### salida



## Ownership

### Mantenedor

Katty Samaniego (ksamanie@bancoestado.cl)

- ### Business Owner
    - Unidad de Negocio:
    - Responsable:
    - Contacto:
- ### Equipos
  - Proyecto: 
    - Product Owner: 
    - Contacto: 
    - Arquitecto de la App: 
    - Contacto: 
    - Scrum Master:
    - Contacto: 
    - Desarrolladores:

    - Qa:
