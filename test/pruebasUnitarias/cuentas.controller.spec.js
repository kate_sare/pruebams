"use strict";

// Imports
const test = require("ava");
const { cuentas } = require("../../src/controllers/cuentas.controller");

// Tests

test("Request OK", async t => {
  let req = {};
  let res = {
    sendCalledWith: "",
    statusCode: null,
    status: function(code) {
      this.statusCode = code;
      return this;
    },
    send: function(args) {
      this.sendCalledWith = args;
      return this;
    }
  };
  await cuentas(req, res);

  t.is(res.statusCode, 200);
});
