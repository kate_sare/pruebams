"use strict";

// Imports
const test = require("ava");
const { clientes } = require("../../src/controllers/clientes.controller");

// Tests

test("Request OK", async t => {
  let req = {};
  let res = {
    sendCalledWith: "",
    statusCode: null,
    status: function(code) {
      this.statusCode = code;
      return this;
    },
    send: function(args) {
      this.sendCalledWith = args;
      return this;
    }
  };
  await clientes(req, res);

  t.is(res.statusCode, 200);
});
